class CreateIkes < ActiveRecord::Migration[6.1]
  def change
    create_table :ikes do |t|
      t.references :article, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
