Rails.application.routes.draw do
  resources :pins
  devise_for :users
  root "articles#index"
  resources :articles do
    resources :comments
    resources :likes
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
