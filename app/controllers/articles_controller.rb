class ArticlesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create edit update destroy] 

  def index
    authorize Article
    @articles = Article.ordered.with_authors
    @articles = Article.ordered.with_authors.paginate( :page => params[:page], per_page: 2).search(params[:query])
  end

  def show_pub
    
     if params[:commit] == "Public"
     @articles = Article.ordered.with_authors.paginate(:page => params[:page], per_page:
    2).public_articles
    
     elsif params[:commit] == "Private"
     @articles = Article.ordered.with_authors.paginate(:page => params[:page], per_page:
    2).private_articles
    
     else
     @articles = Article.ordered.with_authors.paginate(:page => params[:page], per_page:
    2).public_articles

    end
    end

  def show
    @article = Article.find(params[:id])
    authorize @article
  end
  def new
    authorize Article
    @article = Article.new
  end

  def create
    authorize Article
    @article = Article.new(article_params)
    @article.author = current_user
   
    if @article.save
      redirect_to @article
    else
      render :new, status: :unprocessable_entity
    end
  end
  def edit
    @article = Article.find(params[:id])
    authorize @article
  end

  def update
    @article = Article.find(params[:id])
    authorize @article
    
    

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit, status: :unprocessable_entity
    end
  end
  def destroy
    @article = Article.find(params[:id])
    authorize @article
    @article.destroy

    redirect_to root_path
  end
  private
  def article_params
    params.require(:article).permit(:title, :body, :status, :image)
  end

end

