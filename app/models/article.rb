class Article < ApplicationRecord
    belongs_to :author, class_name: 'User'
    include Visible
    has_one_attached :image
    has_many :comments, dependent: :destroy
    has_many :likes, dependent: :destroy
    validates :title, presence: true
    validates :body, presence: true, length: { minimum: 10 }

    scope :public_articles, ->(status = "public"){
     where('status = ?',"#{status}")
     }
    scope :archived_articles, ->(status = "archived"){
     where('status = ?',"#{status}")
     }
    scope :private_articles, ->(status = "private"){
      where('status = ?',"#{status}")
      }

    scope :ordered, ->(direction = :desc) {order(created_at: direction)}
    scope :with_authors, -> {includes(:author)}
    scope :search, ->(query) do
        return if query.blank?
        where('title LIKE ?', "%#{query.squish}%")
      end
end
